
# Changelog

## Trunk

* readme: add author company

## Trunk

* package: support node version above 6
* readme: update sample

## Version 0.0.1

* package: original commit
